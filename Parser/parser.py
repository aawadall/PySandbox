from pyparsing import ZeroOrMore, Regex
# Using https://stackoverflow.com/questions/27303977/split-string-at-double-quotes-and-box-brackets
parser = ZeroOrMore(Regex(r'\[[^]]*\]') | Regex(r'"[^"]*"') | Regex(r'[^ ]+'))

verbs = {"CREATE": 0, "DELETE": 1}
keywords = {"PROJECT": "K0"}
command = -1
args = []
s = 'CREATE PROJECT [Project One] WITH ROOT TASK [Root Task]'
for i in parser.parseString(s):
    print(i.replace('"', "").replace("[", "").replace("]", ""))
    if i in verbs.keys():
        print("{} is a verb of index {}".format(i, verbs[i]))
        command = verbs[i]
    else:
        args.append(i)

print("Command: {} with args:".format(command))
print(args)
